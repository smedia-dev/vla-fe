import { ReactNode } from "react";
import Image from "next/image";

import {
  Box,
  Flex,
  useDisclosure,
  useColorModeValue,
  useColorMode,
} from "@chakra-ui/react";

export default function NavBar() {
  const { colorMode, toggleColorMode } = useColorMode();
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Box px={4} style={{backgroundColor:'rgb(86 126 192 / 26%)'}}>
        <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
          <Box>
            <a
              href="https://smedia.ca/"
              target="_blank"
            >
              <span >
                <Image
                  src="/smedialogo.png"
                  alt="smedia Logo"
                  width={110}
                  height={26}
                />
              </span>
            </a>
          </Box>


        </Flex>
      </Box>
    </>
  );
}
